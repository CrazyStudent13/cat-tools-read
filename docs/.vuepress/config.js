let currentEnv = process.env.NODE_ENV == 'development'
console.log(currentEnv,'测试__')
module.exports = {
    head: [
      ['link', { rel: 'icon', href: '/logo-cat.png' }]
    ],
    title: 'crazystudent13的个人博客',
    description: 'crazystudent13的个人博客',
    base: currentEnv ? '/': "/",
    dest: './public',  
    theme: 'reco',
    locales: {
        '/': {
          lang: 'zh-CN'
        }
    },
    themeConfig: {
        lastUpdated: '最后修改时间', // string | boolean
        nav: [
            { text: '首页', link: '/' },
            // { text: '生活', link: '/guide/guide_zh' },
            // { text: '前端', link: '/doc/string/isNullorUndefined' }
        ],
        // sidebar: [
        //     {
        //       title: "",
        //       // path: '/other/ComputeStrWidth',
        //       children: [
        //         { title: "computeStrWidth", path: "/doc/other/computeStrWidth" },
        //         { title: "createRandomCode", path: "/doc/other/createRandomCode" },
        //         { title: "exportExcelFile", path: "/doc/other/exportExcelFile" }
        //       ],
        //     },
        //   ]
    }
  }

  