---
pageClass: guide
---

## 前言

为了解决多个项目现场一些前端小工具方法使用不同步的问题，特地整合这个工具包。

主要包括了对字符串，数组，数据字典，时间格式，以及一些其他的工具。

目前不算太完善，希望大家持续拍砖。

本项目会持续更新，一直到整个前端生态发生了革命性的变化。



## 快速上手

引入cat-tools

```
npm i cat-tools --save-dev
```



在 main.js 中写入以下内容

```javascript
import Vue from 'vue'
import router from './router'


import { catTools } from 'cat-tools'
Vue.prototype.$devTools = catTools

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
```



有些时候，我们内部自己写了一些方法，这些方法我们自己已经定义了，但又想使用cat-tools工具

那么，假设我在./utils/devTools.js文件中引入了对应的文件，我推荐按照以下写法，将本地和线上的文件综合

```javascript
import Vue from 'vue'
import router from './router'

// 引入本地的JS工具包
import localDevTools from '@/utils/devTools.js'
// 引入cat-tools工具包
import { catTools } from 'cat-tools'

// 融合两种写法
const devTools = Object.assign(catTools,devToolsTemp)

// 挂在到vue原型上
Vue.prototype.$devTools = devTools
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
```
