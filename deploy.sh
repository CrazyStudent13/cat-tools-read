# 确保脚本抛出遇到的错误
set -e

# 全局配置打开cmd
git config --global user.name "CrazyStudent13"
git config --global user.email "1076535690@qq.com"

# 查看
git config user.name
git config user.email

# 打包生成静态文件
npm run build

# 进入打包好的文件夹
# cd public

# 打开项目根目录，终端下配置
git status 检查项目状态
git add .  将所有文件添加到暂存区
git commit -m "提交,打包,远端推送"  提交暂存区的文件
git status

# 覆盖式地将本地仓库发布至github，因为发布不需要保留历史记录
git remote add origin https://gitee.com/CrazyStudent13/cat-tools-read.git
# 将本地master分支的文件推送到远程master分支，第一次推送时需加 -u
git push -u origin master

cd -